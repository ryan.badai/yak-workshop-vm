# Useful if VM is imported from OVA on a windows host, allows to setup 2nd network interface for SSH into the virtualmachine
Set-Alias -Name vboxmanage -Value 'C:\Program Files\Oracle\VirtualBox\VBoxManage.exe'
$interface_number = (VBoxManage hostonlyif create) -replace "[^0-9]" , ''
VBoxManage hostonlyif ipconfig "VirtualBox Host-Only Ethernet Adapter #$interface_number" --ip="10.10.10.$interface_number" --netmask="255.255.255.0"
vboxmanage modifyvm yak-workshop --host-only-adapter2="VirtualBox Host-Only Ethernet Adapter #$interface_number"