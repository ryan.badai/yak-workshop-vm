Vagrant.configure("2") do |yakvm|
    yakvm.vm.provider "virtualbox" do |v|
        v.memory = 2048
        v.cpus = 2
        v.customize ['modifyvm', :id, '--nested-hw-virt', 'on'] # Nested virtualization necessary for docker in VM
        v.name = "yak-workshop"
    end
    
    yakvm.vm.box = "generic/rocky9"
    yakvm.vm.hostname = "yak-workshop"
    yakvm.vm.network "private_network", ip: "10.10.10.100"
    yakvm.vm.provision "shell", inline: <<-SHELL
      # Enable ssh password auth
      sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config    
      systemctl restart sshd.service
      # Add yak user with sudo rights
      useradd student
      usermod --password $(echo student | openssl passwd -1 -stdin) student
      usermod -aG wheel student
      usermod --password $(echo root | openssl passwd -1 -stdin) root
      # Install docker 
      dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
      dnf -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
      sudo systemctl --now enable docker
      # Install yak
      docker pull registry.gitlab.com/yak4all/yak:latest
      echo 'export MY_LOCAL_CONFIGURATION_DIR=/home/student/yak/inventory' >  /home/student/.bashrc 
      sudo -u student mkdir -p /home/student/yak/inventory
      # Create script to start yak 
      sudo -u student touch /home/student/start_yak.sh
      echo 'docker run -it --rm --name yak --pull always -v ${MY_LOCAL_CONFIGURATION_DIR}:/workspace/yak/configuration/infrastructure -e YAK_DEV_UID=$(id -u) -e YAK_DEV_GID=$(id -g) registry.gitlab.com/yak4all/yak:latest bash' > /home/student/start_yak.sh
      usermod -aG docker student
      localectl set-keymap ch-fr
      SHELL
  end