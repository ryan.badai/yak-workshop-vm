# yak-workshop-vm

This repository's goal is to setup a VM on a Windows machine to use YaK.

## Installation
Prerequisite : 
- You must have virtualbox installed on your machine.


If you want to use an other hypervisor such as Hyper-V you may want to convert the OVA file if needed or adapt the Vagrantfile of the VM to rebuild the VM properly for your hypervisor (as far as I know vagrant does not support creating network interfaces or nat rules for hyper v, thus the provisionning script in the vagrantfile won't work.)

### Using OVA 

Download the OVA and import it in virtualbox :
[OVA link](https://dbiservicesoffice365-my.sharepoint.com/:u:/g/personal/ryan_badai_dbi-services_com/Ebfos50UHkJOu99pw9o0yA0BNgx1KwBjOVxeiz04j9SjRg)

If you want to be able to ssh into the VM, you must then create a NAT rule on network interface1 to forward your host os desired tcp port to tcp port 22 of guest, or create a loopback network interface on your host os (host only virtualbox interface) and assign it to the VM's 2nd slot; you can use [my script](https://gitlab.com/ryan.badai/yak-workshop-vm/-/blob/main/setup_network.ps1) if necessary.




### Using vagrant

- Step 1 : Install Vagrant if you don't already have it. Vagrant is a wrapper for Virtualbox / other hypervisors and allows to create VMs images that can be pushed to a repository in similar way as docker; we'll use it to setup our yak environment easily : 
    - Option 1 : Use winget package manager :
        > winget install Hashicorp.Vagrant 
    - Option 2 : Download latest installer : 
        > https://developer.hashicorp.com/vagrant/downloads

- Step 2 : cd into the yak-workshop-repository (your terminal must be able to reach the Vagrantfile) and launch the virtual machine with vagrant (some setup steps require admin rights) : 

    > vagrant up


# Use the VM 
    - Here are the parameters of the VM :
        - IP address : 10.10.10.100 (reachable from host via a   loopback interface created by vagrant up command or manually)
        - user : student
        - password : student

- ssh into the newly created virtual machine : 
    > ssh student@10.10.10.100
    >
    > student@10.10.10.100's password: student

-  Launch and start using yak : 
    > sh start_yak.sh


